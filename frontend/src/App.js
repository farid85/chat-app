import { Routes, Route } from "react-router-dom";
import { Button, ButtonGroup } from "@chakra-ui/react";
import Home from "./pages/Home";
import Chats from "./pages/Chats";
import "./App.css";

function App() {
  return (
    <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/chats" element={<Chats />} />
    </Routes>
  );
}

export default App;
