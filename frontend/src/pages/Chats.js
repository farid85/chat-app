import React, { useContext, useEffect, useState } from "react";
import { ChatState } from "../contexts/ChatProvider";
import { Box } from "@chakra-ui/layout";
import SideDrawer from "../components/Chat/SideDrawer";
import MyChats from "../components/MyChats";
import ChatBox from "../components/ChatBox";

const Chats = () => {
  // const {user } = useContext(ChatContext) // insted of this use bottom line
  const { user } = ChatState(); //short way
  console.log(user);

  const [fetchAgain, setFetchAgain] = useState(false)

  return (
    <div>
      {user && <SideDrawer />}
      <Box d="flex" justifyContent="space-between" w="100%" h="91.5vh" p="10px">
        {user && <MyChats  fetchAgain={fetchAgain} setFetchAgain={setFetchAgain} />}
        {user && (
          <ChatBox fetchAgain={fetchAgain} setFetchAgain={setFetchAgain}/>
        )}
      </Box>
    </div>
  );
};

export default Chats;
