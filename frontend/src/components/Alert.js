import { useToast } from "@chakra-ui/toast";

const Alert = ({ title, status, description }) => {
  const toast = useToast();
  return toast({
    title: title,
    status: status,
    description: description,
    duration: 5000,
    isClosable: true,
    position: "top",
  });
};

export default Alert;
