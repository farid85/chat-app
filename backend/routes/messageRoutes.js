const express = require("express");
const {
  sendMessage,
  allMessages
} = require("../controllers/messageControllers");
const { protect } = require("../middlewares/authMiddleware");

const router = express.Router();


router.get("/:chatId", protect, allMessages);
router.post("/", protect, sendMessage);


module.exports = router;
