const express = require("express");
const {
  registerUser,
  loginUser,
  getUsers,
} = require("../controllers/userControllers");
const { protect } = require("../middlewares/authMiddleware");

const router = express.Router();


router.post("/login", loginUser);
router.post("/register", registerUser);
router.get("/", protect, getUsers);

module.exports = router;
